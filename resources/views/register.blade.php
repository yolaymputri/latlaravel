<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
</head>
<body>
    <h1> Buat Account Baru! </h1>
    <h2> Sign Up Form </h2>
    <form action="/post" method="POST">
        @csrf
       <label> First Name: </label> <br> <br>
       <input type ="text" name="nama"> <br> <br>
       <label> Last Name: </label> <br> <br>
       <input type="text" name="namabelakang"> <br> <br>
       <label> Gender: </label> <br> <br>
       <input type="radio" name="JK"> Male  <br> <br>
       <input type="radio" name="JK"> Female  <br> <br>
       <input type="radio" name="JK"> Other  <br> <br>
       <label> Nationality: </label>
       <select name="Na" >
           <option value="Indonesian"> Indonesian</option>
           <option value="Other"> Other </option>
       </select> <br> <br>
       <label> Languange Spoken:</label> <br> <br>
       <input type="checkbox"> Bahasa Indonesia <br> <br>
       <input type="checkbox"> English <br> <br>
       <input type="checkbox"> Other <br> <br>
       <label> Bio: </label> <br> <br>
       <textarea name="tulisan " cols="30" rows="10"></textarea> <br> <br>
       <input type="submit" value="Sign Up">
    </form>
</body>
</html>